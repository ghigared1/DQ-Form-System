package test.application.controller.helper_classes;

import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import application.controller.helper_classes.HTTPPostHandler;
import application.controller.menu_controllers.MainMenuController;
import application.model.Company;
import application.model.PersonalInformation;
import application.model.User;

public class HTTPPostHandlerTest {
	MainMenuController mainMenuController = new MainMenuController();

	@Before
	public void initMainMenuController(){
		mainMenuController.setUser(generateTestUser());
	}

	public User generateTestUser(){
		User user = new User();
		user.setCompanyApplyingForInformation(generateTestCompany());
		user.setPersonalInformation(generateTestPersonalInformation());
		return user;
	}

	public Company generateTestCompany(){
		return new Company(
				"Test Company",
				"123 Fake St.",
				"El Paso",
				"TX",
				"79938"
		);
	}

	public PersonalInformation generateTestPersonalInformation(){
		int id = LocalTime.now().getNano();
		return new PersonalInformation(
				id+"_FN",
				id+"_LN",
				"",
				"000000000",
				"10/17/1989",
				"0000000000"
			);
	}

	@Test
	public void isUploading() {
		try {
			mainMenuController.uploadForm();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
