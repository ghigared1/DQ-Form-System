package application.controller.ui_component_controllers;

import application.model.Address;
import application.model.enums.error_messages.EmploymentEntryErrorCode;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class BusinessAddressController {
	
    @FXML private TextField zip;
    @FXML private TextField address;
    @FXML private TextField city;
    @FXML private ComboBox<?> state;
	
	public  EmploymentEntryErrorCode getErrorMessage() {
		if (address.getText().isEmpty() && !address.isDisabled())
			return EmploymentEntryErrorCode.PHYSICAL_ADDRESS;
		if (city.getText().isEmpty() && !city.isDisabled())
			return EmploymentEntryErrorCode.CITY; 
		if (state.getValue() == null && !state.isDisabled())
			return EmploymentEntryErrorCode.STATE; 
		if ((zip.getText().length() != 5 || zip.getText().contains("_")) && !zip.isDisabled())
			return EmploymentEntryErrorCode.ZIP; 
		return EmploymentEntryErrorCode.VALID;
	}
	
	public String getPhysicalAddress(){
		return address.getText(); 
	}

	public String getCity(){
		return city.getText(); 
	}
	
	public String getState(){
		return state.getValue().toString(); 
	}
	
	public String getZipCode(){
		return zip.getText(); 
	}
	
	public Address getAddress(){
		if (address != null)
			return new Address(address.getText(), city.getText(), state.getValue().toString(), zip.getText()); 
		else
			return new Address("Test Address", "Test City", "test State", "test zip"); 
	}
	
	public void setTestData(){
		address.setText("123 Fake Street");
		city.setText("El Paso");
		zip.setText("12341");
		state.getSelectionModel().selectFirst();
	}
	
	public void clearData() {
		zip.setText("_____"); 
		address.clear();
		city.clear(); 
		state.getSelectionModel().clearSelection();
	}
}
