package application.controller.ui_component_controllers;

import application.controller.Interfaces.Clearable;
import application.controller.screen_controllers.UserInformationController;
import application.model.enums.error_messages.EmergencyContactErrorCode;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class EmergencyContactController implements Clearable{
	private UserInformationController parentController; 
	
    @FXML private TextField name;
    @FXML private TextField phoneNumber;
    @FXML private TextField relationship;
    
    @FXML
    public void deleteRow(MouseEvent event) {
    		parentController.removeEmergencyContact(this); 
    }
	
	public EmergencyContactErrorCode getErrorMessage(){
		if (name.getText().trim().isEmpty())
			return EmergencyContactErrorCode.NAME; 
		if (phoneNumber.getText().length() != 16 || phoneNumber.getText().contains("_"))
			return EmergencyContactErrorCode.NUMBER; 
		if (relationship.getText().trim().isEmpty())
			return EmergencyContactErrorCode.RELATIONSHIP; 
		return EmergencyContactErrorCode.VALID; 
	}
	
	public void setParentController(UserInformationController parentController) {
		this.parentController = parentController; 
	}
	
	public String getName(){
		return name.getText().trim(); 
	}
	
	public String getPhoneNumber(){
		return phoneNumber.getText(); 
	}
	
	public String getRelationship(){
		return relationship.getText().trim(); 
	}
	
	public void setTestData(){
		name.setText("Emergency contact name");
		phoneNumber.setText("(123) 412 - 3413");
		relationship.setText("Emergency Contact Relationship");
	}

	@Override
	public void clearFields() {
		name.clear();
		phoneNumber.clear(); 
		relationship.clear();
		
	}
}
