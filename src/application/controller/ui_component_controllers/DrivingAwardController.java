package application.controller.ui_component_controllers;

import java.time.LocalDate;

import application.controller.screen_controllers.CriminalInformationController;
import application.controller.screen_controllers.EducationInformationController;
import application.model.Award;
import application.model.enums.error_messages.DrivingAwardErrorCode;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class DrivingAwardController {
	private EducationInformationController parentController; 
	
	@FXML private TextField awardDescription;
	@FXML private TextField issuingCompany;
	@FXML private DatePicker dateCompleted;
	  
	@FXML private ImageView deleteButton;

	@FXML
	public void deleteRow(MouseEvent event) {
		parentController.deleteDrivingAward(this); 
	}
	
	public void setParentController(EducationInformationController parentController) {
		this.parentController = parentController; 
	}
	  
	public DrivingAwardErrorCode getErrorMessage(){
		if (awardDescription.getText().trim().isEmpty())
			return DrivingAwardErrorCode.AWARD_DESCRIPTION; 
		if (issuingCompany.getText().trim().isEmpty())
			return DrivingAwardErrorCode.ISSUING_COMPANY; 
		if (dateCompleted.getValue() == null)
			return DrivingAwardErrorCode.DATE_COMPLETED; 
		if (dateCompleted.getValue().isAfter(LocalDate.now()))
			return DrivingAwardErrorCode.FUTURE_DATE; 
		return DrivingAwardErrorCode.VALID;
	}

	public String getAwardDescription(){
		  return awardDescription.getText().trim(); 
	}
	  
	public String getIssuingCompany(){
		return issuingCompany.getText().trim(); 
	}

	public String getDateCompleted(){
		return dateCompleted.getValue().toString(); 
	}
	  
	public Award generateAwardObject(){
		return new Award(awardDescription.getText(), dateCompleted.getValue().toString(), issuingCompany.getText()); 
	}
	  
	public void setTestData(){
		awardDescription.setText("Description of award");
		issuingCompany.setText("Issuing company of award");
		dateCompleted.setValue(LocalDate.now());
	}
}
