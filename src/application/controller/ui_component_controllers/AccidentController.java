package application.controller.ui_component_controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import application.controller.helper_classes.TextFormatterSingleton;
import application.controller.screen_controllers.CriminalInformationController;
import application.model.Accident;
import application.model.enums.error_messages.AccidentError;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class AccidentController implements Initializable{
	private CriminalInformationController parentController; 
	
    @FXML private TextField numberOfInjuries;
    @FXML private DatePicker dateOfOccurance;
    @FXML private TextArea natureOfAccident;
    @FXML private TextField numberOfFatalities;
    
	@FXML private ImageView deleteButton;

	@FXML
	public void deleteRow(MouseEvent event) {
		parentController.deleteAccident(this);  
	}
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		TextFormatterSingleton.getInstance().setIntegerFilter(numberOfInjuries);
		TextFormatterSingleton.getInstance().setIntegerFilter(numberOfFatalities); 
	}
	
	public void setParentController(CriminalInformationController parentController) {
		this.parentController = parentController; 
	}
    
    public AccidentError getErrorMessage(){
    	if (dateOfOccurance.getValue() == null)
    		return AccidentError.DATE_OF_OCCURANCE; 
    	if (dateOfOccurance.getValue().isAfter(LocalDate.now()))
    		return AccidentError.FUTURE_DATE; 
    	if (numberOfInjuries.getText().trim().isEmpty())
    		return AccidentError.NUMBER_OF_INJURIES; 
    	if (numberOfFatalities.getText().trim().isEmpty())
    		return AccidentError.NUMBER_OF_FATALITIES; 
    	if (natureOfAccident.getText().trim().isEmpty())
    		return AccidentError.NATURE_OF_ACCIDENT; 
    	return AccidentError.VALID; 
    }
    
    public String getNumberOfInjuries(){
    	return numberOfInjuries.getText(); 
    }
    
    public String getDateOfOccurance(){
    	return dateOfOccurance.getValue().toString(); 
    }
    
    public String natureOfAccident(){
    	return natureOfAccident.getText(); 
    }
    
    public Accident generateAccidentObject(){
    	return new Accident(dateOfOccurance.getValue().toString(), natureOfAccident.getText(), numberOfInjuries.getText(), numberOfFatalities.getText()); 
    }
    
    public void setTestData(){
    	numberOfInjuries.setText("2");
    	numberOfFatalities.setText("2");
    	natureOfAccident.setText("Set nature of accident");
    	dateOfOccurance.setValue(LocalDate.now());
    }
}
