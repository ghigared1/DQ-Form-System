package application.controller.screen_controllers;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import application.controller.Interfaces.Clearable;
import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import application.controller.ui_component_controllers.EmploymentEntryController;
import application.model.PreviousEmployer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;

public class HistoryOfEmploymentController implements Initializable, Navigable, Clearable{
	
    @FXML private VBox employersContainer;
    private ArrayList<EmploymentEntryController> employmentEntryControllerList; 
    
    @FXML Button addEmploymentButton; 
    
    @FXML private CheckBox noEmploymentHistory;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		employmentEntryControllerList = new ArrayList<EmploymentEntryController>(); 
		setNoEmploymentCheckBoxOnChangeListener();
	}
	
	public void setTestData(){
		addEmploymentInformation(null);
		employmentEntryControllerList.get(0).setTestData(); 
	}
	
	private void setNoEmploymentCheckBoxOnChangeListener(){
		noEmploymentHistory.selectedProperty().addListener(new ChangeListener<Boolean>() {
	        public void changed(ObservableValue<? extends Boolean> ov,
	            Boolean old_val, Boolean new_val) {
		        	if(new_val == true){
		        		if (!employmentEntryControllerList.isEmpty()){
			        		employmentEntryControllerList.clear();
			        		employersContainer.getChildren().clear(); 
		        		}
		        		
		        		addEmploymentButton.setDisable(true);
		        	}else{
		        		addEmploymentButton.setDisable(false);
		        	}
	        }
	    });
	}
	
    @FXML
    void addEmploymentInformation(ActionEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/EmploymentEntry.fxml"));
   	 	try {
   	 		employersContainer.getChildren().add(loader.load());
   	 		
   	 		EmploymentEntryController temp = (EmploymentEntryController) loader.getController(); 
   	 		
   	 		if(!employmentEntryControllerList.isEmpty())
   	 			temp.setDate(employmentEntryControllerList.get(employmentEntryControllerList.size()-1).getFromDate());
   	 		
   	 		temp.setParentController(this);
   	 		employmentEntryControllerList.add(temp); 
   	 		
		} catch (IOException e) {
			e.printStackTrace();
		} 
    }

	@Override
	public String getErrorMessage() {
		
		if (employmentEntryControllerList.isEmpty() && !noEmploymentHistory.isSelected())
			return "Please provide us your work history or check to box to indicate that you have no work history"; 
		
		for (int i = 0; i < employmentEntryControllerList.size(); i++){
			switch (employmentEntryControllerList.get(i).getErrorMessage()){
			case COMPANY: 
				return "Please provide a valid company name at entry position number: " + (i+1); 
			case SUPERVISOR_NAME: 
				return "Please provide a valid supervisor name at entry position number: " + (i+1); 
			case PHONE_NUMBER: 
				return "Please provide a valid phone number at entry position number: " + (i+1); 
			case PHYSICAL_ADDRESS: 
				return "Please provide a valid address at entry position number: " + (i+1); 
			case CITY: 
				return "Please provide a valid city at entry position number: " + (i+1); 
			case STATE: 
				return "Please provide a valid state at entry position number: " + (i+1); 
			case ZIP: 
				return "Please provide a vaild zip code at entry position number: " + (i+1); 
			case POSITION_HELD: 
				return "Please provide a valid position held at entry position number: " + (i+1); 
			case FROM_DATE: 
				return "Please provide a valid from date at entry position number: " + (i+1); 
			case TO_DATE: 
				return "Please provide a valid to date at entry position number: " + (i+1); 
			default: 
				break; 
			}
		}
		
		if(!noEmploymentHistory.isSelected())
			if(!isAtLeast10Years() )
				return "Please provide at least 10 years of working history, if you have any less please specify so, by selecting unemployed"; 
		
		return "valid";
	}
	
	private boolean isAtLeast10Years(){
		LocalDate firstDate; 
		LocalDate lastDate; 
		
		if (employmentEntryControllerList.size() == 1){
			firstDate = employmentEntryControllerList.get(0).getFromDate(); 
			lastDate = employmentEntryControllerList.get(0).getToDate(); 
		}else{
			firstDate = employmentEntryControllerList.get(employmentEntryControllerList.size()-1).getFromDate(); 
			lastDate = employmentEntryControllerList.get(0).getToDate(); 
		}
		
		if (Duration.between(firstDate.atTime(0, 0), lastDate.atTime(0, 0)).toDays() < 3650)
			return false; 
		return true; 
		
	}
	
	public ArrayList<PreviousEmployer> getEmploymentHistory(){
		ArrayList<PreviousEmployer> temp = new ArrayList<PreviousEmployer>(); 
		
		for(int i = 0; i < employmentEntryControllerList.size(); i++){
			temp.add(employmentEntryControllerList.get(i).generatePreviousEmployer()); 
		}
		
		
		return temp; 
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		// TODO Auto-generated method stub
	}

	@Override
	public void clearFields() {
		noEmploymentHistory.setSelected(false);
		employersContainer.getChildren().clear();
		employmentEntryControllerList.clear();		
	}
	
	public void deleteEmploymentEntry(EmploymentEntryController employmentEntry) {
		employersContainer.getChildren().remove(employmentEntryControllerList.indexOf(employmentEntry)); 
		employmentEntryControllerList.remove(employmentEntry); 
	}
}