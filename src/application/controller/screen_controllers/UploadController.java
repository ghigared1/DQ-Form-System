package application.controller.screen_controllers;

import java.net.URL;
import java.util.ResourceBundle;

import application.controller.Interfaces.Navigable;
import application.controller.helper_classes.HTTPPostHandler;
import application.controller.helper_classes.PDFWriter;
import application.controller.menu_controllers.MainMenuController;
import application.model.User;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class UploadController extends HTTPPostHandler implements Initializable, Navigable{ 
	private MainMenuController mainController; 
	
    @FXML private Label progressLabel;
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
		
	@Override
	public String getErrorMessage() {
		return ""; 
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		this.mainController = mainController; 
		currentUser = mainController.getUser(); 
	}

	@Override
	public void handleSuccessCase() {
		mainController.showNextScreen(null); 
	}

	@Override
	public void handleFailureCase() {
		// TODO Auto-generated method stub
	}
}
