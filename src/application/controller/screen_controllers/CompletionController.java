package application.controller.screen_controllers;

import java.net.URL;
import java.util.ResourceBundle;

import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class CompletionController implements Initializable, Navigable{
	private MainMenuController mainController; 
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
    @FXML
    public void startFormOver(ActionEvent event) {
    		mainController.showNextScreen(event);
    }

	@Override
	public String getErrorMessage() {
		return ""; 
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		this.mainController = mainController; 
	}
}
