package application.controller.helper_classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

import application.model.User;

public class PDFWriter {
	private final String PRE_TEMP_PATH = "/resources/temp_forms";
	private final String PRE_TEMPLATE_PATH = "/resources/forms";
	private User user;

	private PDDocument page1;
	private PDDocument page2;
	private PDDocument page3;
	private PDDocument page4;
	private PDDocument page5;
	private PDDocument page6;
	private PDDocument page7;
	private PDDocument page8;
	private PDDocument page9;
	private PDDocument page10;
	private PDDocument page11;
	private PDDocument page12;
	private PDDocument page13;
	private PDDocument page14;
	private PDDocument page15;
	private PDDocument page16;
	private PDDocument page17;

	private File file1;
	private File file2;
	private File file3;
	private File file4;
	private File file5;
	private File file6;
	private File file7;
	private File file8;
	private File file9;
	private File file10;
	private File file11;
	private File file12;
	private File file13;
	private File file14;
	private File file15;
	private File file16;
	private File file17;

	private ArrayList<PDDocument> consentFormList;

	public PDFWriter(){
		page1 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_1.pdf");
		page2 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_2.pdf");
		page3 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_3.pdf");
		page4 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_4.pdf");
		page5 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_5.pdf");
		page6 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_6.pdf");
		page7 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_7.pdf");
		page8 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_8.pdf");
		page9 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_9.pdf");
		page10 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_10.pdf");
		page11 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_11.pdf");
		page12 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_12.pdf");
		page13 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_13.pdf");
		page14 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_14.pdf");
		page15 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_15.pdf");
		page16 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_16.pdf");
		page17 = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/page_17.pdf");

		consentFormList = new ArrayList<PDDocument>();
	}

	public void setUser(User user){
		this.user = user;
	}

	public void write(){
		generateTempDirectories();
		fillOutPage1();
		fillOutPage2();
		fillOutPage3();
		fillOutPage4();
		fillOutPage5();

		if(user.getHistoryOfEmployment().size() > 4)
			fillOutPage6();

		fillOutPage7();
		fillOutPage8();
		fillOutPage9();
		fillOutPage10();
		fillOutPage11();
		fillOutPage12();
		fillOutPage13();
		fillOutPage14();
		fillOutPage15();
		fillOutPage16();
		fillOutPage17();

		generateConsentForms();
		attachImages();
		mergePages();
	}

	public void generateTempDirectories() {
		File dir = new File("temp_files/pdf_forms");

	    boolean successful = dir.mkdirs();

	    if (successful) {
	    		generateTempFile(file1, "temp_files/pdf_forms/temp_page1.pdf");
	    		generateTempFile(file2, "temp_files/pdf_forms/temp_page2.pdf");
	    		generateTempFile(file3, "temp_files/pdf_forms/temp_page3.pdf");
	    		generateTempFile(file4, "temp_files/pdf_forms/temp_page4.pdf");
	    		generateTempFile(file5, "temp_files/pdf_forms/temp_page5.pdf");
	    		generateTempFile(file6, "temp_files/pdf_forms/temp_page6.pdf");
	    		generateTempFile(file7, "temp_files/pdf_forms/temp_page7.pdf");
	    		generateTempFile(file8, "temp_files/pdf_forms/temp_page8.pdf");
	    		generateTempFile(file9, "temp_files/pdf_forms/temp_page9.pdf");
	    		generateTempFile(file10, "temp_files/pdf_forms/temp_page10.pdf");
	    		generateTempFile(file11, "temp_files/pdf_forms/temp_page11.pdf");
	    		generateTempFile(file12, "temp_files/pdf_forms/temp_page12.pdf");
	    		generateTempFile(file13, "temp_files/pdf_forms/temp_page13.pdf");
	    		generateTempFile(file14, "temp_files/pdf_forms/temp_page14.pdf");
	    		generateTempFile(file15, "temp_files/pdf_forms/temp_page15.pdf");
	    		generateTempFile(file16, "temp_files/pdf_forms/temp_page16.pdf");
	    		generateTempFile(file17, "temp_files/pdf_forms/temp_page17.pdf");
	    }
	}

	//TODO: Move into it's own IO Singleton Class
	public void generateTempFile(File file, String path) {
		file = new File(path);

		try{
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void fillOutPage1(){
		PDAcroForm acroForm = page1.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Name", user.getPersonalInformation().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());

		PDFBoxSingleton.getInstance().savePDDocument(page1, "temp_files/pdf_forms/temp_page1.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page1);
	}

	public void fillOutPage2(){
		PDAcroForm acroForm = page2.getDocumentCatalog().getAcroForm();
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());

		PDFBoxSingleton.getInstance().savePDDocument(page2, "temp_files/pdf_forms/temp_page2.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page2);
	}

	public void fillOutPage3(){
		PDAcroForm acroForm = page3.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().savePDDocument(page3, "temp_files/pdf_forms/temp_page3.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page3);
	}

	public void fillOutPage4(){
		PDAcroForm acroForm = page4.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyAddress1", user.getCompany().getAddress().getPhysicialAddressString());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyAddress2", user.getCompany().getAddress().getCityStateZipString());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Date", DateSingleton.getInstance().getDate());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Name", user.getPersonalInformation().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PhoneNumber", user.getPersonalInformation().getPhoneNumber());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CurrentAddress", user.getCurrentAddress().toString());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "YearsAtCurrentAddress", user.getYearsAtCurrentAddress());

		for(int i = 0; i < user.getPreviousAddressList().size(); i++){
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddress"+(i+1), user.getPreviousAddressList().get(i).toString());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddressYears"+(i+1), Double.toString(user.getPreviousAddressList().get(i).getYearsAtResidence()));
		}

		if (user.getPreviousAddressList().isEmpty()){
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddress1", "N/A");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddressYears1", "N/A");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddress2", "N/A");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddressYears2", "N/A");
		}

		if (user.hasBondDenied()){
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Bond", "YES");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "BondDate", user.getBondDeniedReason());
		} else{
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Bond", "NO");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "BondDate", "N/A");
		}

		if (user.hasViolation()){
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Violations", "YES");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "ViolationExplanation", user.getViolationReason());
		}else {
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Violations", "NO");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "ViolationExplanation", "N/A");
		}

		if (user.getPreviousAddressList().size() == 1){
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddress2", "N/A");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddressYears2", "N/A");
		}

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Position", user.getPosition());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "RateOfPay", user.getRate());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "ReferredBy", user.getReferred());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Availability", user.getAvailability());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Relatives", user.getRelatives());

		if (user.getEducation().getEducationLevel().toLowerCase().contains("Grade".toLowerCase()))
			PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "school" + user.getEducation().getEducationLevel().substring(8, user.getEducation().getEducationLevel().length()));
		else
			PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "college" + user.getEducation().getEducationLevel().substring(10, user.getEducation().getEducationLevel().length()));

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SchoolName", user.getEducation().getNameOfLastSchoolAttended() + "\t" + user.getEducation().getSchoolAddressString());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SpecialCourses", user.getEducation().getSpecialCoursesString());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DrivingAwards", user.getEducation().getDrivingAwardsString());

		PDFBoxSingleton.getInstance().savePDDocument(page4, "temp_files/pdf_forms/temp_page4.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page4);
	}

	public void fillOutPage5(){
		PDAcroForm acroForm = page5.getDocumentCatalog().getAcroForm();
		for(int i = 0; i < user.getHistoryOfEmployment().size(); i++){
			if (i < 4) {
				if (user.getHistoryOfEmployment().get(i).getStatus().equals("Employed")) {
					PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName"+(i+1), user.getHistoryOfEmployment().get(i).getCompanyName());
					PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Address"+(i+1), user.getHistoryOfEmployment().get(i).getAddress().toString());
					PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Phone"+(i+1), user.getHistoryOfEmployment().get(i).getPhoneNumber());
					PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Position"+(i+1), user.getHistoryOfEmployment().get(i).getPositionHeld());
				}
				else
					PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName"+(i+1), user.getHistoryOfEmployment().get(i).getStatus());


				PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Supervisor"+(i+1), user.getHistoryOfEmployment().get(i).getSupervisorName());
				PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "From"+(i+1), user.getHistoryOfEmployment().get(i).getFromDate());
				PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "To"+(i+1), user.getHistoryOfEmployment().get(i).getToDate());

				if (user.getHistoryOfEmployment().get(i).isMotorCarrier())
					PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "SafetyRegulationsY"+(i+1));
				else
					PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "SafetyRegulationsN"+(i+1));

				if (user.getHistoryOfEmployment().get(i).isDrugTestingRequired())
					PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "DrugsY"+(i+1));
				else
					PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "DrugsN"+(i+1));
			}
		}

		PDFBoxSingleton.getInstance().savePDDocument(page5, "temp_files/pdf_forms/temp_page5.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page5);
	}

	public void fillOutPage6(){
		PDAcroForm acroForm = page6.getDocumentCatalog().getAcroForm();
		for(int i = 4; i < user.getHistoryOfEmployment().size(); i++){
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName"+(i+1), user.getHistoryOfEmployment().get(i).getCompanyName());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Supervisor"+(i+1), user.getHistoryOfEmployment().get(i).getSupervisorName());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Address"+(i+1), user.getHistoryOfEmployment().get(i).getAddress().toString());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Phone"+(i+1), user.getHistoryOfEmployment().get(i).getPhoneNumber());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Position"+(i+1), user.getHistoryOfEmployment().get(i).getPositionHeld());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "From"+(i+1), user.getHistoryOfEmployment().get(i).getFromDate());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "To"+(i+1), user.getHistoryOfEmployment().get(i).getToDate());

			if (user.getHistoryOfEmployment().get(i).isMotorCarrier())
				PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "SafetyRegulationsY"+(i+1));
			else
				PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "SafetyRegulationsN"+(i+1));

			if (user.getHistoryOfEmployment().get(i).isDrugTestingRequired())
				PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "DrugsY"+(i+1));
			else
				PDFBoxSingleton.getInstance().fillOutCheckbox(acroForm, "DrugsN"+(i+1));
		}

		PDFBoxSingleton.getInstance().savePDDocument(page6, "temp_files/pdf_forms/temp_page6.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page6);
	}

	public void fillOutPage7(){
		PDAcroForm acroForm = page7.getDocumentCatalog().getAcroForm();
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "FlatbedYears", user.getEducation().getFlatbedYears());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "BusesYears", user.getEducation().getBusesYears());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "StraightTrucksYears", user.getEducation().getStraightTruckYears());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "TractorsYears", user.getEducation().getTractorYears());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SemiYears", user.getEducation().getSemiYears());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DoublesYears", user.getEducation().getDoublesYears());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "OtherText", "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "OtherYears", "N/A");

		for (int i = 0; i < user.getCriminalHistory().getAccidentList().size(); i++)
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Accident" + (i+1), user.getCriminalHistory().getAccidentList().get(i).toString());

		if (user.getCriminalHistory().getAccidentList().isEmpty())
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Accident1", "N/A");

		for(int i = 0; i < user.getCriminalHistory().getTrafficViolationList().size(); i++)
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "TrafficViolation"+(i+1) , user.getCriminalHistory().getTrafficViolationList().get(i).toString());

		if (user.getCriminalHistory().getTrafficViolationList().isEmpty())
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "TrafficViolation1", "N/A");

		if (user.getCriminalHistory().hasSuspension()){
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Suspension", "YES");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SuspensionExplanation", user.getCriminalHistory().getSuspensionDescription());
		} else {
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Suspension", "NO");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SuspensionExplanation", "N/A");
		}

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "EmergencyName1", !user.getEmergencyList().isEmpty() ? user.getEmergencyList().get(0).getName() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "EmergencyPhone1", !user.getEmergencyList().isEmpty() ? user.getEmergencyList().get(0).getPhone() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "EmergencyRelationship1" ,  !user.getEmergencyList().isEmpty() ? user.getEmergencyList().get(0).getRelationship() : "N/A");

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "EmergencyName2", user.getEmergencyList().size() > 1 ? user.getEmergencyList().get(1).getName() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "EmergencyPhone2", user.getEmergencyList().size() > 1 ? user.getEmergencyList().get(1).getPhone() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "EmergencyRelationship2" , user.getEmergencyList().size() > 1 ? user.getEmergencyList().get(1).getRelationship() : "N/A");

		PDFBoxSingleton.getInstance().savePDDocument(page7, "temp_files/pdf_forms/temp_page7.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page7);
	}

	public void fillOutPage8(){
		PDAcroForm acroForm = page8.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Date", DateSingleton.getInstance().getDate());

		PDFBoxSingleton.getInstance().savePDDocument(page8, "temp_files/pdf_forms/temp_page8.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page8);
	}

	public void fillOutPage9(){
		PDAcroForm acroForm = page9.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Name", user.getPersonalInformation().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CDLNumber", user.getDLInfo().getdlNumber());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Date", DateSingleton.getInstance().getDate());

		PDFBoxSingleton.getInstance().savePDDocument(page9, "temp_files/pdf_forms/temp_page9.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page9);
	}

	public void fillOutPage10(){
		PDAcroForm acroForm = page10.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLNumber", user.getDLInfo().getdlNumber());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLState", user.getDLInfo().getdlState());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLExpiration", user.getDLInfo().getdlExpirationDate());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Date", DateSingleton.getInstance().getDate());

		PDFBoxSingleton.getInstance().savePDDocument(page10, "temp_files/pdf_forms/temp_page10.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page10); 	}

	public void fillOutPage11(){
		PDAcroForm acroForm = page11.getDocumentCatalog().getAcroForm();
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Name", user.getPersonalInformation().getName());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Address", user.getCurrentAddress().getPhysicialAddressString());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CityState", user.getCurrentAddress().getCityStateString());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Zip", user.getCurrentAddress().getZipCode());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddress1", !user.getPreviousAddressList().isEmpty() ? user.getPreviousAddressList().get(0).getPhysicialAddressString() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousCityState1", !user.getPreviousAddressList().isEmpty() ? user.getPreviousAddressList().get(0).getCityStateString() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousZip1" ,  !user.getPreviousAddressList().isEmpty() ? user.getPreviousAddressList().get(0).getZipCode() : "N/A");

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousAddress2", user.getPreviousAddressList().size() > 1 ? user.getPreviousAddressList().get(1).getPhysicialAddressString() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousCityState2", user.getPreviousAddressList().size() > 1  ? user.getPreviousAddressList().get(1).getCityStateString() : "N/A");
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousZip2" ,  user.getPreviousAddressList().size() > 1  ? user.getPreviousAddressList().get(1).getZipCode() : "N/A");

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SSN", user.getPersonalInformation().getSsn());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DOB", user.getPersonalInformation().getDob());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLNumber", user.getDLInfo().getdlNumber());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLState", user.getDLInfo().getdlState());

		//TODO: TRAFFIC VIOLATIONS AND CRIMINAL

		PDFBoxSingleton.getInstance().savePDDocument(page11, "temp_files/pdf_forms/temp_page11.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page11);
	}

	public void fillOutPage12(){
		PDAcroForm acroForm = page12.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Date", DateSingleton.getInstance().getDate());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DriverName", user.getPersonalInformation().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLNumber", user.getDLInfo().getdlNumber());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLState", user.getDLInfo().getdlState());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DLExpirationDate", user.getDLInfo().getdlExpirationDate());

		PDFBoxSingleton.getInstance().savePDDocument(page12, "temp_files/pdf_forms/temp_page12.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page12);
	}

	public void fillOutPage13(){
		PDAcroForm acroForm = page13.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "DriverName", user.getPersonalInformation().getName());

		PDFBoxSingleton.getInstance().savePDDocument(page13, "temp_files/pdf_forms/temp_page13.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page13);
	}

	public void fillOutPage14(){
		PDAcroForm acroForm = page14.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Name", user.getPersonalInformation().getName());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SSN", user.getPersonalInformation().getSsn());
		PDFBoxSingleton.getInstance().	fillOutTextField(acroForm, "DLNumber", user.getDLInfo().getdlNumber());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "dateOne", user.getHoursOfService().getDayOne());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "dateTwo", user.getHoursOfService().getDayTwo());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "dateThree", user.getHoursOfService().getDayThree());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "dateFour", user.getHoursOfService().getDayFour());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "dateFive", user.getHoursOfService().getDayFive());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "dateSix", user.getHoursOfService().getDaySix());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "dateSeven", user.getHoursOfService().getDaySeven());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "hoursOne", user.getHoursOfService().getTimeOne());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "hoursTwo", user.getHoursOfService().getTimeTwo());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "hoursThree", user.getHoursOfService().getTimeThree());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "hoursFour", user.getHoursOfService().getTimeFour());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "hoursFive", user.getHoursOfService().getTimeFive());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "hoursSix", user.getHoursOfService().getTimeSix());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "hoursSeven", user.getHoursOfService().getTimeSeven());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "total", user.getHoursOfService().generateTotalTimeString());

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "relievedDate", user.getHoursOfService().getRelievedDate());
		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "relievedTime", user.getHoursOfService().getRelievedTime());

		PDFBoxSingleton.getInstance().savePDDocument(page14, "temp_files/pdf_forms/temp_page14.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page14);
	}

	public void fillOutPage15(){
		PDAcroForm acroForm = page15.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());

		PDFBoxSingleton.getInstance().savePDDocument(page15, "temp_files/pdf_forms/temp_page15.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page15);
	}

	public void fillOutPage16(){
		PDAcroForm acroForm = page16.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());

		PDFBoxSingleton.getInstance().savePDDocument(page16, "temp_files/pdf_forms/temp_page16.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page16);
	}

	public void fillOutPage17(){
		PDAcroForm acroForm = page17.getDocumentCatalog().getAcroForm();

		PDFBoxSingleton.getInstance().savePDDocument(page17, "temp_files/pdf_forms/temp_page17.pdf");
		PDFBoxSingleton.getInstance().closePDDocument(page17);
	}

	private void generateConsentForms(){
		for (int i = 0; i < user.getHistoryOfEmployment().size(); i++){
			PDDocument consentPage = PDFBoxSingleton.getInstance().loadPDFTemplate(PRE_TEMPLATE_PATH + "/consent_form.pdf");
			PDAcroForm acroForm = consentPage.getDocumentCatalog().getAcroForm();

			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName", user.getCompany().getName());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "StreetAddress1", user.getCompany().getAddress().getPhysicialAddressString());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "StreetAddress2", user.getCompany().getAddress().getCityStateZipString());

			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "Date", DateSingleton.getInstance().getDate());

			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyName2", user.getCompany().getName());

			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "PreviousEmployer", user.getHistoryOfEmployment().get(i).getCompanyName());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyNumber", user.getHistoryOfEmployment().get(i).getPhoneNumber());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyAddress", user.getHistoryOfEmployment().get(i).getAddress().getPhysicialAddressString());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyFax", "N/A");
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyCity", user.getHistoryOfEmployment().get(i).getAddress().getCity());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyState", user.getHistoryOfEmployment().get(i).getAddress().getState());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "CompanyZip", user.getHistoryOfEmployment().get(i).getAddress().getZipCode());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "SSN", user.getPersonalInformation().getSsn());
			PDFBoxSingleton.getInstance().fillOutTextField(acroForm, "ApplicationName", user.getPersonalInformation().getName());

			try {
			    consentPage.save("temp_files/pdf_forms/consent_temp_"+i+".pdf");
				consentPage.close();

			} catch (IOException e) {
				System.out.println("Generate Consent form: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private void attachImages(){
		PDDocument document = new PDDocument();

	      //Retrieving the page
		PDPage my_page = new PDPage();

	      //Creating PDImageXObject object
	    PDImageXObject pdImage = null;
	    PDImageXObject pdImage2 = null;

	    try {
			pdImage = PDImageXObject.createFromFile(user.getDLInfo().getFrontOfLicenseAbsolutePath() ,document);
			pdImage2 = PDImageXObject.createFromFile(user.getDLInfo().getBackOfLicenseAbsolutePath(), document);
		} catch (IOException e) {
			System.out.println("xObject:" + e.getMessage());
		}

	      //creating the PDPageContentStream object
	      PDPageContentStream contents = null;
		try {
			contents = new PDPageContentStream(document, my_page);
		} catch (IOException e) {
			System.out.println("Contents: " + e.getMessage());
		}

	      //Drawing the image in the PDF document
	      try {
			contents.drawImage(pdImage, 70, 50);
			contents.drawImage(pdImage2, 200, 200);
		} catch (IOException e) {
			System.out.println("Draw Image: " + e.getMessage());
		}

	      //Closing the PDPageContentStream object
	      try {
			contents.close();
		} catch (IOException e) {
			System.out.println("Close Contents: " + e.getMessage());
		}

		  document.addPage(my_page);

	      //Saving the document
	      try {
			document.save("temp_files/pdf_forms/images.pdf");
		} catch (IOException e) {
			System.out.println("Documen save: " + e.getMessage());
		}

	      //Closing the document
	      try {
			document.close();
		} catch (IOException e) {
			System.out.println("Document close: " + e.getMessage());
		}
	}

	private void mergePages(){
		PDFMergerUtility mergePdf = new PDFMergerUtility();
		mergePdf.setDestinationFileName("temp_files/pdf_forms/dq_file.pdf");
		try {
			mergePdf.addSource("temp_files/pdf_forms/temp_page1.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page2.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page3.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page4.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page5.pdf");

			if(user.getHistoryOfEmployment().size() > 4)
				mergePdf.addSource("temp_files/pdf_forms/temp_page6.pdf");

			mergePdf.addSource("temp_files/pdf_forms/temp_page7.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page8.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page9.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page10.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page11.pdf");

			for(int i = 0; i < user.getHistoryOfEmployment().size(); i++)
				mergePdf.addSource("temp_files/pdf_forms/consent_temp_" + i + ".pdf");

			mergePdf.addSource("temp_files/pdf_forms/temp_page12.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page13.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page14.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page15.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page16.pdf");
			mergePdf.addSource("temp_files/pdf_forms/temp_page17.pdf");

			mergePdf.addSource("temp_files/pdf_forms/images.pdf");

			mergePdf.mergeDocuments(null);
		} catch (FileNotFoundException e) {
			System.out.println("Merge Pages Error File not Found: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Merge Pages Error IOException: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
