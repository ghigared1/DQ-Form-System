package application.controller.helper_classes;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

public class PDFBoxSingleton {
	private static PDFBoxSingleton instance = null;

	protected PDFBoxSingleton() {
	   // Exists only to defeat instantiation.
	}

    public static PDFBoxSingleton getInstance() {
       if(instance == null) {
          instance = new PDFBoxSingleton();
       }
       return instance;
    }

  	public PDDocument loadPDFTemplate(String PDFPath){
  		try {
  			return PDDocument.load(getClass().getResourceAsStream(PDFPath));
  		} catch (InvalidPasswordException e) {
  			System.out.println("Load PDF TEmplate Error Invalid Password: " + e.getMessage());
  			e.printStackTrace();
  			return null;
  		} catch (IOException e) {
  			System.out.println("Load PDF TEmplate Error IOException: " + e.getMessage());
  			e.printStackTrace();
  			return null;
  		}
  	}

  	public PDDocument loadPDFTemplate(File file){
  		try {
			return PDDocument.load(file);
		} catch (InvalidPasswordException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
  	}

  	public void fillOutTextField(PDAcroForm page, String fieldName, String fieldText){
  		PDTextField textField = (PDTextField) page.getField(fieldName);
  		try {
  			textField.setValue(fieldText);
  		} catch (IOException e) {
  			System.out.println("FillOutTextField: IOException: " + e.getMessage());
  			e.printStackTrace();
  		}
  	}

  	public void fillOutCheckbox(PDAcroForm page, String fieldName){
  		PDCheckBox checkbox = (PDCheckBox) page.getField(fieldName);
          try {
  			checkbox.check();
  		} catch (IOException e) {
  			// TODO: handle exception
  			e.printStackTrace();
  		}
  	}

  	public void savePDDocument(PDDocument document, String path){
  		try {
			document.save(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	}

  	public void closePDDocument(PDDocument document){
  		try {
			document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	}
}
