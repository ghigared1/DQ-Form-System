package application.controller.helper_classes;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateSingleton {
	private static DateSingleton instance = null;

	protected DateSingleton() {
	   // Exists only to defeat instantiation.
	}

    public static DateSingleton getInstance() {
       if(instance == null) {
          instance = new DateSingleton();
       }
       return instance;
    }

	public String getDate(){
		return new SimpleDateFormat("dd-MM-yyyy").format(new Date());
	}
}
