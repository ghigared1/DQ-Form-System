package application.model.enums.error_messages;

public enum EmploymentEntryErrorCode {
	COMPANY, 
	SUPERVISOR_NAME, 
	PHONE_NUMBER, 
	PHYSICAL_ADDRESS, 
	CITY, 
	STATE, 
	ZIP, 
	POSITION_HELD, 
	FROM_DATE, 
	TO_DATE, 
	VALID; 
}
