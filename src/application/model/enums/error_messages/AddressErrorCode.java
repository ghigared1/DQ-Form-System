package application.model.enums.error_messages;

public enum AddressErrorCode {
	ADDRESS(""), 
	CITY(""), 
	STATE(""), 
	ZIP(""), 
	YEARS_AT_RESIDENCE(""), 
	PREVIOUS_ADDRESSES_NEEDED(""), 
	VALID("");
	
	private String errorMessage;

    AddressErrorCode(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String errorMessage() {
        return errorMessage;
    }
}
