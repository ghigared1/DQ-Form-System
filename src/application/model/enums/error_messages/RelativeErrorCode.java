package application.model.enums.error_messages;

public enum RelativeErrorCode {
	NAME, 
	RELATION,
	VALID
}
