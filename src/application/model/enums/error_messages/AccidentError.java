package application.model.enums.error_messages;

public enum AccidentError {
	NUMBER_OF_INJURIES, 
	DATE_OF_OCCURANCE, 
	FUTURE_DATE,
	NATURE_OF_ACCIDENT, 
	NUMBER_OF_FATALITIES,
	VALID
}
