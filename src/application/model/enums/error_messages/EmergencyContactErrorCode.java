package application.model.enums.error_messages;

public enum EmergencyContactErrorCode {
	NAME, 
	NUMBER, 
	RELATIONSHIP, 
	VALID
}
