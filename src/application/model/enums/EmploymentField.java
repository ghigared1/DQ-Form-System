package application.model.enums;

public enum EmploymentField {
	POSITION, RATE_OF_PAY, NAME_OF_REFERENCE, AVAILABILITY_DATE
}
