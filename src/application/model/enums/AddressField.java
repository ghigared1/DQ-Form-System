package application.model.enums;

public enum AddressField {
	ADDRESS, CITY, STATE, ZIP, YEARS_AT_RESIDENCE
}
