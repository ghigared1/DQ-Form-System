package application.model;

import java.util.ArrayList;

public class DrivingExperience {
	private int yearsDrivingFlatbeds; 
	private int yearsDrivingBuses; 
	private int yearsDrivingStraightTrucks; 
	private int yearsDrivingTractors; 
	private int yearsDrivingSemiTrailers; 
	private int yearsDrivingDoubles; 
	//TODO: implement a way to track other using a hashmap
	
	private ArrayList<Accident> previousAccidentList; 
	
	private boolean hadSuspension; 
	private String reasonForSuspension; 
}
