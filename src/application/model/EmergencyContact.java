package application.model;

public class EmergencyContact {
	private String name; 
	private String phone; 
	private String relationship;
	
	public EmergencyContact(String name, String phone, String relationship) {
		super();
		this.name = name;
		this.phone = phone;
		this.relationship = relationship;
	} 
	
	public String getName(){
		return name; 
	}
	
	public String getPhone(){
		return phone; 
	}
	
	public String getRelationship(){
		return relationship; 
	}
}
