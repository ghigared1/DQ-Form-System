package application.model;

public class Address {
	private String physicalAddress; 
	private String city; 
	private String state; 
	private String zipCode;
	private double yearsAtResidence; 
	
	public Address(String physicalAddress, String city, String state, String zipCode) {
		this.physicalAddress = physicalAddress;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
	} 
	
	public Address(String physicalAddress, String city, String state, String zipCode, double yearsAtResidence) {
		super();
		this.physicalAddress = physicalAddress;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.yearsAtResidence = yearsAtResidence;
	} 
	
	public String toString(){
		return physicalAddress + " " + city + " " + state + " " + zipCode; 
	}
	
	public String getPhysicialAddressString(){
		return physicalAddress; 
	}
	
	public String getCityStateZipString(){
		return city + " " + state + " " + zipCode; 
	}
	
	public double getYearsAtResidence(){
		return yearsAtResidence;
	}
	
	public String getCityStateString(){
		return city + " " + state; 
	}
	
	public String getZipCode(){
		return zipCode; 
	}
	
	public String getCity(){
		return city; 
	}
	
	public String getState(){
		return state; 
	}

}
