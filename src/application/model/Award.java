package application.model;

public class Award {
	private String awardDescription; 
	private String dateCompleted; 
	private String issuingCompany; 
	
	public Award (String awardDescription, String dateCompleted, String issuingCompany){
		this.awardDescription = awardDescription; 
		this.dateCompleted = dateCompleted; 
		this.issuingCompany = issuingCompany; 
	}
	
	public String toString(){
		return "[ "+awardDescription+" - "+issuingCompany+" - "+dateCompleted+" ]"; 
	}
}
