package application.model;

public class Relative {
	private String name; 
	private String relationship;
	
	public Relative(String name, String relationship) {
		super();
		this.name = name;
		this.relationship = relationship;
	} 
	
	public String toString(){
		return "[" + name + ":" + relationship + "]"; 
	}
}
