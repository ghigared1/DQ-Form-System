package application.model;

import java.util.ArrayList;

public class Education {
	private String flatbedYears; 
	private String busesYears; 
	private String straightTruckYears; 
	private String tractorYears; 
	private String semiYears; 
	private String doublesYears; 
	
	private String otherText; 
	private String otherYears; 
	
	private String educationLevel; 
	
	private String nameOfLastSchoolAttended; 
	private Address addressOfLastSchoolAttended; 
	
	private ArrayList<Course> completedCourseList; 
	private ArrayList<Award> awardsList; 
	
	public Education(String educationLevel, String nameOfLastSchoolAttended, ArrayList<Course> completedCourseList, ArrayList<Award> awardsList, Address addressOfLastSchoolAttended,
			String flatbedYears, String busesYears, String straightTruckYears, String tractorYears, String semiYears, String doublesYears){
		this.educationLevel = educationLevel; 
		this.nameOfLastSchoolAttended = nameOfLastSchoolAttended; 
		this.completedCourseList = completedCourseList; 
		this.awardsList = awardsList; 
		this.addressOfLastSchoolAttended = addressOfLastSchoolAttended; 
		this.flatbedYears = flatbedYears; 
		this.busesYears = busesYears; 
		this.straightTruckYears = straightTruckYears; 
		this.tractorYears = tractorYears; 
		this.semiYears = semiYears; 
		this.doublesYears = doublesYears; 
	}
	
	public String getNameOfLastSchoolAttended(){
		return nameOfLastSchoolAttended; 
	}
	
	public String getSpecialCoursesString(){
		String temp = ""; 
		
		if (completedCourseList.isEmpty())
			return "N/A"; 
		
		for (int i = 0; i < completedCourseList.size(); i++)
			temp += completedCourseList.get(i).toString(); 
		return temp; 
	}
	
	public String getDrivingAwardsString(){
		String temp = ""; 
		
		if (awardsList.isEmpty())
			return "N/A"; 
		
		for (int i = 0; i < awardsList.size(); i++)
			temp+=awardsList.get(i).toString(); 
		return temp; 
	}

	public String getFlatbedYears() {
		return flatbedYears;
	}

	public void setFlatbedYears(String flatbedYears) {
		this.flatbedYears = flatbedYears;
	}

	public String getBusesYears() {
		return busesYears;
	}

	public String getStraightTruckYears() {
		return straightTruckYears;
	}

	public String getTractorYears() {
		return tractorYears;
	}

	public String getSemiYears() {
		return semiYears;
	}

	public String getDoublesYears() {
		return doublesYears;
	}
	
	public String getEducationLevel(){
		return educationLevel; 
	}
	
	public String getSchoolAddressString(){
		return addressOfLastSchoolAttended.toString(); 
	}
}
